package Bookmarker::Model::URLs;
use Mojo::Base -base;

use warnings;
use strict;

use Mojo::UserAgent;

use DateTime;

use Carp ();
use Data::Dumper;

has postgres => sub { Carp::croak 'db is required' };
has log      => sub { Carp::croak 'log is required' };
has config   => sub { Carp::croak 'config is required' };

sub get_bookmarks {
    my $self = shift;
    my $params = shift;

    my $like = $params->{go_to} ? '%\_' . $params->{go_to} . '\_%' : '%%';
    
    my $urls = $self
	->postgres
	->db
	->query('select id, nick, channel, timestamp, url, context from all_urls where context like ?',
                 $like
        )
	->hashes;

    return $urls;
};

sub add_bookmark {
    my ($self, $uname, $url, $keywords) = @_;

    #$self->log->debug("id in model: $id");

    my $ua = Mojo::UserAgent->new;
    my $res = $ua->get($url);

    my $body;
    # valid url?
    if (defined ($res->original_remote_address)) {
        if ($res->result->is_success) {
	    $body = $res->result->body;
        }
    }
	
    #$self->log->debug($res->body());

    my $id = $self
	->postgres
	->db
	->query('select logurl(?, ?, ?, ?, ?)',
                $uname, 'web', $url, $keywords, $body
               )->hash->{logurl};
    $self->log->debug("from logurl id: " . $id);

    return { id => $id };
};

sub update_bookmark {
    my ($self, $id, $uname, $url, $keywords) = @_;

    $id = $self
	->postgres
	->db
	->query('select updateurl(?, ?, ?, ?)',
                $id, $uname, $url, $keywords
               )->hash->{updateurl};
    $self->log->debug("from update_bookmark id: " . $id);

    return { id => $id };
}

sub delete_bookmark {
    my ($self, $id) = @_;

    my $cnt = $self
	->postgres
	->db
	->query('delete from url where id = ?', $id
               );

    $self->log->debug("delete bookmark id: $id");

    # TODO:  some error handling to address failed deletes
    return { id => $id };
}

sub get_content {
    my ($self, $id) = @_;

    my $resp = $self
	->postgres
	->db
	->query('select content from url where id = ?', $id
               )->hash->{content};

    #$self->log->debug("content for $id: " . $resp);

    return { content => $resp };
}

1;
