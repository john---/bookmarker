package Bookmarker::Controller::Main;

use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;

# This action will render a template
sub urls {
    my $self = shift;

    $self->render;
}

sub items {
    my $self = shift;

    my $urls = $self->model->get_bookmarks();

    $self->render( json =>  { data => $urls->to_array } );
}

sub save {
    my $self = shift;

    #print "in controller id: " . $self->param('id') . "\n";

    my $resp;
    if ( ! $self->param('id') ) {
        $resp = $self->model->add_bookmark(
                                               $self->param('uname'),
                                               $self->param('url'),
                                               $self->param('keywords')
                                             );
    } else {
        $resp = $self->model->update_bookmark(
                                               $self->param('id'),
                                               $self->param('uname'),
                                               $self->param('url'),
                                               $self->param('keywords')
                                            );
    }

    $self->render(json => $resp);
}

sub delete {
    my $self = shift;

    my $resp = $self->model->delete_bookmark( $self->param('id') );

    $self->render(json => $resp);
}

sub content {
    my $self = shift;

    my $resp = $self->model->get_content( $self->param('id') );

    $self->render(json => $resp);
}

sub go_there {
    my $self = shift;

    (my $short = $self->param('goto')) =~ s/\/$//;

    my $bkmrk = $self->model->get_bookmarks( { go_to => $short } );

    if ($bkmrk->size == 1) {
        my $url = $bkmrk->first->{url};
        $self->redirect_to($url);
    } else { # nothing was returned or more than one go link was returned
        $self->redirect_to("/?search=$short");
     }
}

1;
