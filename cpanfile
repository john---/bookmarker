requires 'DBI';
requires 'Mojo::Pg';
requires 'DateTime';
requires 'B::Hooks::EndOfScope';
requires 'namespace::clean';
requires 'Sub::Identify';
requires 'IO::Socket::SSL';