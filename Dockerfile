FROM perl:latest

WORKDIR /opt/app

#COPY . .  # this is needed only for deploying self-contained container
COPY cpanfile .
RUN cpanm --installdeps -n .

CMD [ "perl", "/usr/local/bin/morbo", "./script/bookmarker", "daemon" ]
#CMD [ "perl", "./script/bookmarker", "daemon" ]
#CMD [ "perl", "/usr/local/bin/morbo", "./script/bookmarker", "daemon", "--listen",  "https://*:443?cert=/etc/ssl/certs/server.pem&key=/etc/ssl/private/server.key"]
