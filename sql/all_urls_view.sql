CREATE OR REPLACE VIEW public.all_urls AS
 SELECT
    n.name AS nick,
    c.name AS channel,
    to_char(u."timestamp", 'Dy DD Mon YY HH24:MI TZ'::text) AS timestring,
    u."timestamp",
    u.url,
    u.context,
    u.content,
    u.id
   FROM url u
     JOIN nick n ON n.id = u.nick_id
     JOIN channel c ON c.id = u.channel_id
