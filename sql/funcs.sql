CREATE OR REPLACE FUNCTION logurl(text, text, text, text, text) RETURNS url.id%TYPE
    AS '
	DECLARE
		key		url.id%TYPE;
		nick_in		ALIAS FOR $1;
		channel_in	ALIAS FOR $2;
		url_in		ALIAS FOR $3;
		context_in	ALIAS FOR $4;
		content_in	ALIAS FOR $5;
	BEGIN
		key := nextval(''url_id_seq'');
		INSERT INTO url (id, nick_id, channel_id, url, context, content)
		VALUES (key, getnick(nick_in), getchannel(channel_in), url_in, context_in, content_in);
		RETURN key;
	END;
'
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION updateurl(integer, text, text, text) RETURNS url.id%TYPE
    AS '
	DECLARE
		id_in		ALIAS FOR $1;
		nick_in		ALIAS FOR $2;
		url_in		ALIAS FOR $3;
		context_in	ALIAS FOR $4;
	BEGIN
		UPDATE url SET nick_id = getnick(nick_in), url = url_in, context = context_in
                where id = id_in;
		RETURN id_in;
	END;
'
    LANGUAGE plpgsql;
