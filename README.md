Bookmarker
==========
Application to manage bookmarks

Dependencies
------------

* Mojolicious
* Postgres
* Modules/Plugins
  * Mojo::Pg
